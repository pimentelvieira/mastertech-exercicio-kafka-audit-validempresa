package com.mastertech.validempresa.consumers;

import com.mastertech.cadempresa.models.Empresa;
import com.mastertech.validempresa.feignclients.dtos.CnpjResponseDTO;
import com.mastertech.validempresa.services.ValidadorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class EmpresaConsumer {

    @Autowired
    private ValidadorService validadorService;

    @KafkaListener(topics = "spec2-william-pimentel-2", groupId = "validgroup")
    public void receber(@Payload Empresa empresa) throws IOException {
        System.out.println("Inicio validador - empresa: " + empresa.getNome());
        CnpjResponseDTO dto = validadorService.validar(empresa.getCnpj());
        if (dto.getCapitalSocial() != null) {
            System.out.println("Fim validador com sucesso - empresa: " + empresa.getNome() + ", resultado: " + dto.getCapitalSocial());
        } else {
            System.out.println("Inicio validador com erro - empresa: " + empresa.getNome());
        }
    }
}
