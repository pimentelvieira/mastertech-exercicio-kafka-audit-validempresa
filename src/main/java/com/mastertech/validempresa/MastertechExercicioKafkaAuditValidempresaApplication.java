package com.mastertech.validempresa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MastertechExercicioKafkaAuditValidempresaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MastertechExercicioKafkaAuditValidempresaApplication.class, args);
	}

}
