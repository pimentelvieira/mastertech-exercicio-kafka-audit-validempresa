package com.mastertech.validempresa.feignclients;

import com.mastertech.validempresa.feignclients.dtos.CnpjResponseDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "validacnpj", url = "https://www.receitaws.com.br/v1/cnpj")
public interface ValidaCnpjClient {

    @GetMapping("/{cnpj}")
    CnpjResponseDTO getByCnpj(@PathVariable String cnpj);
}

