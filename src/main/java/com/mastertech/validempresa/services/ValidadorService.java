package com.mastertech.validempresa.services;

import com.mastertech.validempresa.feignclients.ValidaCnpjClient;
import com.mastertech.validempresa.feignclients.dtos.CnpjResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ValidadorService {

    @Autowired
    private ValidaCnpjClient client;

    public CnpjResponseDTO validar(String cnpj) {
        return client.getByCnpj(cnpj);
    }
}
